import React from 'react';
import { Switch, Route } from 'react-router-dom';

import { Home } from '../home/home';
import { Entertainment } from '../entertainment/entertainment';
import { Restaurants } from '../restaurants/restaurants';

import './content.css';

export function Content() {
  return (
    <div className='content'>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/entertainment">
          <Entertainment />
        </Route>
        <Route path="/restaurants">
          <Restaurants />
        </Route>
      </Switch>
    </div>
  );
}
