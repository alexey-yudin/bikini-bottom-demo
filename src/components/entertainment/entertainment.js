import React, {useEffect, useRef} from 'react';
import './entertainment.css';
import {EntertainmentCard} from '../entertainment-card/entertainment-card';

export function Entertainment() {
  const refContainer = useRef(null);

  useEffect(() => {
    refContainer.current.classList.add('fade-in');
  });

  const entertainmentItems = [
    {
      image: 'https://static.wikia.nocookie.net/spongebob/images/5/51/SpongeBob_LongPants_070.png',
      title: 'Reef Cinema',
      text: 'Reef Cinema (also shortened as The Reef) is a theater in Bikini Bottom near Barg\'N-Mart. It commonly shows Mermaid Man and Barnacle Boy movies. It first appears in the episode "F.U.N."'
    },
    {
      image: 'https://static.wikia.nocookie.net/spongebob/images/d/dd/Gone_235.png',
      title: 'The Seven Seas',
      text: 'The Seven Seas is a movie theater that appears in the episode "SpongeBob LongPants."'
    }
  ];

  return (
    <div ref={refContainer} className='page-container entertainment-container fade-out'>
      <div className='restaurant-inner'>
        <h1 className='page-title'>Entertainment</h1>

        <div className='entertainment-cards-list'>
          {entertainmentItems.map(
            (entertainmentCard, index) => <EntertainmentCard key={index} entertainmentCard={entertainmentCard}/>
          )}
        </div>
      </div>
    </div>
  );
}
