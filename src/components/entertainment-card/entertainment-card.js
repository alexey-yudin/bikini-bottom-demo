import React from 'react';
import './entertainment-card.css';

export function EntertainmentCard(props) {
  const {entertainmentCard} = props;

  return (
    <div className='entertainment-card'>
      <img className='entertainment-image' src={entertainmentCard.image} alt="image"/>
      <h2 className='entertainment-title'>{entertainmentCard.title}</h2>
      <p className='entertainment-text'>{entertainmentCard.text}</p>
    </div>
  );
}
