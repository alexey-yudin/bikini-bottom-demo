import React from 'react';
import { Link } from "react-router-dom";

import './navigation.css';

export function Navigation() {
  return (
    <div className='navigation'>
      <Link to='/' className='navigation-item'>Home</Link>
      <Link to='/entertainment' className='navigation-item'>Entertainment</Link>
      <Link to='/restaurants' className='navigation-item'>Restaurants</Link>
    </div>
  );
}
