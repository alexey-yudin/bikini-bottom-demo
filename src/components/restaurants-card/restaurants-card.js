import React from 'react';
import './restaurants-card.css';

export function RestaurantsCard(props) {
  const {card} = props;

  return (
    <div className='restaurant-card'>
      <img className='restaurant-image' src={card.image} alt=""/>
      <h2>{card.title}</h2>
      <p>{card.text}</p>
    </div>
  );
}
