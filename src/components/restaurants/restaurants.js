import React, {useEffect, useRef} from 'react';
import './restaurants.css';
import {RestaurantsCard} from '../restaurants-card/restaurants-card';

export function Restaurants() {
  const refContainer = useRef(null);

  useEffect(() => {
    refContainer.current.classList.add('fade-in');
  });

  const restaurants = [
    {
      image: 'https://static.wikia.nocookie.net/spongebob/images/6/69/My_Two_Krabses_001.png',
      title: 'Krusty Krab',
      text: 'The Krusty Krab is a fast food restaurant located in Bikini Bottom, founded and owned by Eugene H. Krabs. It is also the most popular restaurant in Bikini Bottom and the most seen restaurant in SpongeBob SquarePants. It first appears in the pilot episode "Help Wanted."'
    },
    {
      image: 'https://static.wikia.nocookie.net/spongebob/images/3/38/Weenie_Hut_Juniors.png',
      title: 'Weenie Hut Jr\'s',
      text: 'Weenie Hut Jr\'s is a restaurant located in Bikini Bottom. It has two other counterparts, Super Weenie Hut Jr\'s and Weenie Hut General. It first appears in the episode "No Weenies Allowed."'
    }
  ];

  return (
    <div ref={refContainer} className='page-container restaurants-container fade-out'>
      <div className='restaurants-inner'>
        <h1 className='page-title'>Restaurants</h1>

        <div className='restaurants-list'>
          {restaurants.map((card, index) => <RestaurantsCard key={index} card={card}/>)}
        </div>
      </div>
    </div>
  );
}
