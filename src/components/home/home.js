import React, {useEffect, useRef} from 'react';
import './home.css';

export function Home() {
  const refContainer = useRef(null);

  useEffect(() => {
    refContainer.current.classList.add('fade-in');
  });

  return (
    <div ref={refContainer} className='page-container home-container fade-out'>
      <h1 className='page-title'>Welcome to Bikini Bottom website!</h1>
    </div>
  );
}
