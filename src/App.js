import React from 'react';

import { Navigation } from './components/navigation/navigation';
import { Content } from './components/content/content';

import {BrowserRouter as Router} from "react-router-dom";

import './styles/page.css';

function App() {
  return (
    <Router>
      <Navigation/>
      <Content/>
    </Router>
  );
}

export default App;
